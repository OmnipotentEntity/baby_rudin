\prompt{Fix $b > 1$.}

\prompt{(a) If $m$, $n$, $p$, $q$ are integers, $n > 0$, $q > 0$, and $r = m/n =
p/q$, prove that}

\[
  {\qty(b^m)}^{1/n} = {\qty(b^p)}^{1/q}.
\]

\prompt{Hence it makes sense to define $b^r = {\qty(b^m)}^{1/n}$.}

\smallskip \hrule \smallskip

First, let us begin by defining exponentiation for negative integers.

\begin{definition} \lthm{neg-ex}
  For $b \ne 0$ and $n$ a positive integer, we define $b^{-n} \equiv {(1/b)}^n$.
  Additionally, we define $b^0 = 1$.
\end{definition}

Now we will show that negative exponents perserve the two properties of
exponents that we care about.  Namely composition through multiplication and
through exponentiation.

\begin{lemma} \lthm{ex-add}
  For $b \ne 0$ and $m$, $n$ positive integers, $b^m b^{-n} = b^{m-n}$
\end{lemma}

We examine three cases with $m \ne 0$ and $n \ne 0$.  First, if $m > n$ then
$b^m {(1/b)}^n = b^{m-n}$ (by the associativity of multiplication and the
definition of the multiplicative inverse). Second, if $n > m$ then $b^m
{(1/b)}^n = {(1/b)}^{n-m}$ (ibid), which is $b^{m-n}$ by the Def.~\rthm{neg-ex}.
Finally, if $m = n$ then $b^m {(1/b)}^n = 1$ which is $b^0$, and because $m=n$,
we have $b^0 = b^{m-n}$.

Next, we must consider the effects of 0; without loss of generality take $m =
0$. From this we have simply that $b^0 b^n = 1 \cdot b^n = b^{0 + n}$.  The left
multiplication is the same as the right, due to commutivity, and of course $b^0
b^0 = 1 = b^{0 + 0}$.

\begin{lemma} \lthm{ex-mult}
  For $b \ne 0$ and $m$, $n$ positive integers, ${\qty(b^m)}^{-n} = b^{-mn}$,
  ${\qty(b^{-m})}^n = b^{-mn}$, and ${\qty(b^{-m})}^{-n} = b^{mn}$.

  Additionally, ${\qty(b^m)}^{n}$ with one or both of $m$, $n$ equal to 0 yields
  $b^0 = 1$ as one might expect.
\end{lemma}

For the first identity,

\begin{align*}
  {\qty(b^m)}^{-n} &= {\qty(\frac{1}{b^m})}^{n} && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= \frac{1}{b^{mn}} && \qqtext{Associativity of Multiplication} \\
  &= b^{-mn}. && \qqtext{By Def.~\rthm{neg-ex}}
\end{align*}

The second,

\begin{align*}
  {\qty(b^{-m})}^{n} &= {\qty({\qty(\frac{1}{b})}^m)}^{n} && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= {\qty(\frac{1}{b^m})}^{n} && \qqtext{Associativity of multiplication and multiplicative identity} \\
  &= \frac{1}{b^{mn}} && \qqtext{Associativity of multiplication} \\
  &= b^{-mn}. && \qqtext{By Def.~\rthm{neg-ex}}
\end{align*}

And the third,

\begin{align*}
  {\qty(b^{-m})}^{-n} &= {\qty({\qty(\frac{1}{b})}^m)}^{-n} && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= {\qty(\frac{1}{b^m})}^{-n} && \qqtext{Associativity of multiplication and multiplicative identity} \\
  &= {\qty(b^m)}^{n} && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= b^{mn} && \qqtext{Associativity of multiplication}
\end{align*}

Next, we have to verify the identity with 0.  First $m = 0$.

\begin{align*}
  {\qty(b^0)}^n &= 1^n && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= 1 && \qqtext{By multiplicative identity} \\
  &= b^0 && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= b^{mn} && \qqtext{Transitive property of equality}
\end{align*}

Next, $n = 0$.

\begin{align*}
  {\qty(b^m)}^0 &= 1 && \qqtext{By Def.~\rthm{neg-ex} (taking the base to be $b^m$)} \\
  &= b^0 && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= b^{mn} && \qqtext{Transitive property of equality}
\end{align*}

Finally, $m = n = 0$.

\begin{align*}
  {\qty(b^0)}^0 &= 1 && \qqtext{By Def.~\rthm{neg-ex} (taking the base to be $b^0$)} \\
  &= b^0 && \qqtext{By Def.~\rthm{neg-ex}} \\
  &= b^{mn} && \qqtext{Transitive property of equality}
\end{align*}

Next, a few corollaries of Book Theorem 1.21.

\begin{corollary} \lthm{self-root}
  Given $x > 0$, and $t$ is a positive integer, $x = {\qty(x^{1/t})}^t$ and $x =
  {\qty(x^t)}^{1/t}$.
\end{corollary}

Per book theorem 1.21, for every real $x > 0$ and every integer $n > 0$ there is
one and only one positive real $y$ such that $y^n = x$.

\begin{align*}
  y &= x^{1/t} && \qqtext{Existence guaranteed by 1.21} \\
  y^t &= x && \qqtext{ibid} \\
  {\qty(x^{1/t})}^t &= x. && \qqtext{Transitive property of equality}
\end{align*}

Similarly,

\begin{align*}
  x^t &= x^t && \qqtext{Reflexive property of equality} \\
  x &= {\qty(x^t)}^{1/t}.
\end{align*}

Where the final line uses the fact that $y^n = x$ implies $y = x^{1/n}$ with the
left $x^t$ standing in for $y^n$ and right $x^t$ standing in for $x$.

\begin{corollary} \lthm{mult-root}
  Given $x > 1$, and $m$ and $n$ positive integers, ${\qty(x^{1/m})}^{1/n} =
  x^{1/mn}$.
\end{corollary}

\begin{align*}
  \mathrm{Let,} \: y^{mn} &= x && \qqtext{Existence guaranteed by 1.21} \\
  x^{1/mn} &= y && \qqtext{By Theorem 1.21} \\
  y^{mn} &= {\qty(y^n)}^m && \qqtext{By Cor.~\rthm{ex-mult}} \\
  x &= {\qty(y^n)}^m && \qqtext{Transitive property of equality} \\
  x^{1/m} &= y^n && \qqtext{By Theorem 1.21} \\
  {\qty(x^{1/m})}^{1/n} &= y && \qqtext{ibid} \\
  {\qty(x^{1/m})}^{1/n} &= x^{1/mn} && \qqtext{Transitive property of equality}
\end{align*}

And finally, a Lemma that will help us in the proof.

\begin{lemma} \lthm{commute-root}
  Given $x > 1$, ${\qty(x^m)}^{1/n} = {\qty(x^{1/n})}^m$ for $m$, $n$ integers,
  and $n > 0$.
\end{lemma}

\begin{align*}
  \mathrm{Let,} \: y^n &= x && \qqtext{Existence guaranteed by 1.21} \\
  \mathrm{Consider} \: {\qty(x^m)}^{1/n}& \\
  {\qty(x^m)}^{1/n} &= {\qty({\qty(y^n)}^m)}^{1/n} && \qqtext{Transitive property of equality} \\
  &= {\qty({\qty(y^m)}^n)}^{1/n} && \qqtext{By Lem.~\rthm{ex-add}} \\
  &= y^m && \qqtext{By Cor.~\rthm{self-root}} \\
  &= {\qty({\qty(y^n)}^{1/n})}^m && \qqtext{ibid} \\
  &= {\qty(x^{1/n})}^m && \qqtext{Transitive property of equality} \\
\end{align*}

Now we are prepared to attack the original problem.

Let $s$ be the least common multiple of $n$ and $q$.  Let $nt = s = qu$.  Note
that because $nt = qu$, it must also be the case that $mt = pu$. Explicitly:

\begin{align*}
  \frac{m}{n} &= \frac{p}{q} && \qqtext{Given} \\
  1 \cdot \frac{m}{n} &= 1 \cdot \frac{p}{q} && \qqtext{Multiplicative identity} \\
  \frac{t}{t} \cdot \frac{m}{n} &= \frac{u}{u} \cdot \frac{p}{q} && \qqtext{Multiplicative inverse} \\
  \frac{mt}{nt} &= \frac{pu}{qu} && \qqtext{Associative and commutative property of multiplication} \\
  \frac{mt}{s} &= \frac{pu}{s} && \qqtext{Transitive property of equality} \\
  s \cdot \frac{mt}{s} &= s \cdot \frac{pu}{s} && \qqtext{Multiplicative property of equality} \\
  mt &= pu && \qqtext{Associative property of multiplication and multiplicative inverse} \\
\end{align*}

Also n.b.\ that $t$ and $u$ are necessarily positive integers, because $n$, $q$,
and $s$ are all positive.

The rest of the proof is just straight-forward fiddling:

\begin{align*}
  {\qty(b^m)}^{1/n} &= {\qty({\qty({\qty(b^m)}^{1/n})}^{t})}^{1/t} && \qqtext{By Cor~\rthm{self-root}} \\
  &= {\qty({\qty({\qty(b^m)}^{t})}^{1/n})}^{1/t} && \qqtext{By Lem.~\rthm{commute-root}} \\
  &= {\qty(b^{mt})}^{1/nt} && \qqtext{By Cors.~\rthm{mult-root} and~\rthm{ex-mult}} \\
  &= {\qty(b^{pu})}^{1/qu} && \qqtext{Transitive property of equality} \\
  &= {\qty({\qty({\qty(b^p)}^{u})}^{1/q})}^{1/u} && \qqtext{By Cors.~\rthm{mult-root} and~\rthm{ex-mult}} \\
  &= {\qty({\qty({\qty(b^p)}^{1/q})}^{u})}^{1/u} && \qqtext{By Lem.~\rthm{commute-root}} \\
  &= {\qty(b^p)}^{1/q} && \qqtext{By Theorem 1.21}
\end{align*}

Completing the proof.  Hence we can now define

\begin{definition} \lthm{rat-ex}
  Let $b > 0$, $r$ be a rational number, and $r = m/n$ with $m$, $n$ integers
  and $n$ positive. We define $b^r \equiv {\qty(b^m)}^{1/n} \equiv
  {\qty(b^{1/n})}^{m}$.
\end{definition}

(Notice, that our definition fixes $b > 0$ rather than $b > 1$.  We only used $b
> 0$ in our proof, rather than the more restrictive $b > 1$, so we are justified
in this definition.)

\smallskip \hrule \smallskip

\prompt{(b) Prove that $b^{r+s} = b^r b^s$ if $r$ and $s$ are rational.}

\smallskip \hrule \smallskip

Let $r = m/n$ and $s = p/q$ with $m$, $n$, $p$, and $q$ integers and $p \ne 0$,
$q \ne 0$. Without loss of generality, we may assume $n > 0$ and $q > 0$.

Note that $r + s = \frac{m}{n} + \frac{p}{q} = \frac{mq + np}{nq}$ by standard
fraction addition properties.

\begin{align*}
  b^r b^s &= b^{m/n} b^{p/q} && \qqtext{Transitive property of equality} \\
  &= {\qty(b^m)}^{1/n} {\qty(b^p)}^{1/q} && \qqtext{By Def.~\rthm{rat-ex}} \\
  &= {\qty({\qty({\qty(b^m)}^q)}^{1/q})}^{1/n} {\qty({\qty({\qty(b^q)}^n)}^{1/n})}^{1/q} && \qqtext{By Cor.~\rthm{self-root}} \\
  &= {\qty(b^{mq})}^{1/nq} {\qty(b^{np})}^{1/nq} && \qqtext{By Lem.~\rthm{ex-mult} and Cor.~\rthm{mult-root}} \\
  &= {\qty(b^{mq} b^{np})}^{1/nq} && \qqtext{By Book Corollary to Theorem 1.21} \\
  &= {\qty(b^{mq + np})}^{1/nq} && \qqtext{By Lem.~\rthm{ex-add}} \\
  &= b^{(mq + np)/nq} && \qqtext{By Def.~\rthm{rat-ex}} \\
  &= b^{r + s} && \qqtext{From the above discussion}
\end{align*}

Which completes our proof, so let's state the result as a theorem so was may use
it later.

\begin{theorem} \lthm{rat-ex-add}
  Let $b > 0$, with $r$ and $s$ rational numbers. $b^r b^s = b^{r+s}$.
\end{theorem}

Before we move on, a small improvement to our previous Corollary

\begin{corollary} \lthm{self-root-rat}
  Given $x > 0$, and $r=m/n$ is a non-zero rational number, with $m$, $n$
  integers, and $m$ non-zero, $n$ positive, $x = {\qty(x^{1/r})}^r$ and $x =
  {\qty(x^r)}^{1/r}$.
\end{corollary}

\begin{align*}
  x &= {\qty(x^n)}^{1/n} && \qqtext{By Cor.~\rthm{self-root}} \\
  &= {\qty({\qty({\qty(x^{1/m})}^m)}^n)}^{1/n} && \qqtext{By Cor.~\rthm{self-root}} \\
  &= {\qty({\qty({\qty(x^{1/m})}^n)}^m)}^{1/n} && \qqtext{By Cor.~\rthm{ex-mult} and commutativity of multiplication} \\
  x &= {\qty(x^{1/r})}^{r} && \qqtext{By Def.~\rthm{rat-ex}} \\
\end{align*}

Note that both $1/r$ and $r$ are rational numbers, so to prove the second part,
we may just choose $1/r$ as our original number.

Additionally, we should prove another property about rational exponents.

\begin{theorem} \lthm{rat-ex-mult}
  Let $b > 0$, with $r$ and $s$ rational numbers. ${\qty(b^r)}^s = b^{rs}$.
\end{theorem}

Let $r = m/n$ and $s = p/q$ with $m$, $n$, $p$, and $q$ integers and $p \ne 0$,
$q \ne 0$. Without loss of generality, we may assume $n > 0$ and $q > 0$.

Note that $rs = mp/nq$ by the associativity and commutivity of multiplication.

This proof will have a similar trajectory to Thm.~\rthm{rat-ex-add}.

\begin{align*}
  {\qty(b^r)}^s &= {\qty({\qty({\qty(b^m)}^{1/n})}^p)}^{1/q} && \qqtext{By Def.~\rthm{rat-ex}} \\
  &= {\qty({\qty({\qty(b^m)}^p)}^{1/n})}^{1/q} && \qqtext{By Lem.~\rthm{commute-root}} \\
  &= {\qty(b^{mp})}^{1/nq} && \qqtext{By Book Corollary to Theorem 1.21 and associativity of multiplication} \\
  &= b^{mp/nq} && \qqtext{By Def.~\rthm{rat-ex}} \\
  &= b^{rs} && \qqtext{Transitive property of equality}
\end{align*}

And a small corollary.

\begin{corollary} \lthm{rat-neg-ex}
  Let $b > 0$, with $r$ a rational number. $b^r = {(1/b)}^{-r}$.
\end{corollary}

\begin{align*}
  b^r &= b^{-1 \cdot -r} && \qqtext{Property of additive inverse vs multiplication} \\
  &= {\qty(b^{-1})}^{-r} && \qqtext{By Thm.~\rthm{rat-ex-mult}} \\
  &= {(1/b)}^{-r} && \qqtext{By Def.~\rthm{neg-ex}}
\end{align*}

These will be useful later, for now, moving on.

\smallskip \hrule \smallskip

\prompt{(c) If $x$ is real, define $B(x)$ to be the set of all numbers $b^t$,
where $t$ is rational and $t \le x$.  Prove that}

\[
  b^r = \sup B(r)
\]

\prompt{when $r$ is rational. Hence it makes sense to define}

\[
  b^x = \sup B(x)
\]

\prompt{for every real $x$.}

\smallskip \hrule \smallskip

So there are actually a few possibilities here to fully prove this for all $b >
0$ and $r$ rational.  Let's start with the degenerate case first.

\textbf{Case 1:} $b = 1$ and $b = 0$, because $1^{1/n}$ for $n$ positive integer
is always 1, similarly $1^n$ for any integer $n$ is also 1.  Similarly, for 0.

This means that for every rational number $r$ it is true that $1^r = 1$ by our
definition.  Therefore, our set $B(r)$ only contains the number 1, and the
$\sup$ of this set is also $1$ for any real number $x$. Similarly, for 0.

\textbf{Case 2:} In this case, we will be dealing with the case where $b > 1$.

There are a number of small things we will need to prove before we can attack
the main result.

First, we will need to prove something that the book wasn't careful about.
Namely, that if $b > 1$ and $n > 1$ then $b^n > b$.  We will be doing this via
an inductive proof. But before we do even that we must show that two real
numbers greater than 1 multiplied together create a number greater than either.

\begin{lemma} \lthm{mult-inc}
  Let $x$ and $y$ be real numbers. If $x \ge y > 1$ then $xy > x \ge y$.
\end{lemma}

We can let $x = 1 + z$ and $y = 1 + w$, and now we have $z \ge w > 0$.

\begin{align*}
  xy &= (1 + z)(1 + w) && \qqtext{Definition of $z$ and $w$} \\
  &= 1 + z + w + zw && \qqtext{Distributive property} \\
  &> 1 + z && \qqtext{$w$ and $wz$ are strictly positive} \\
  &> x && \qqtext{Definition of $x$}
\end{align*}

Now we can begin the proof of this lemma proper.

\begin{lemma} \lthm{ex-inc}
  Given $b > 1$ and $n$ positive integer, then $b^n \ge b$.
\end{lemma}

First, the base case $n = 1$.

\begin{align*}
  b^1 &= b && \qqtext{Definition of exponent} \\
  b^1 &\ge b && \qqtext{Broadening to a less restrictive comparison}
\end{align*}

Then, the inductive case.

\begin{align*}
  b^n &\ge b && \qqtext{Inductive case assumption} \\
  b b^n &> b^n && \qqtext{By Lem.~\rthm{mult-inc}} \\
  b^{n+1} &> b^n && \qqtext{Associativity of multiplication} \\
  b^{n+1} &> b && \qqtext{Transitive property of inequalities} \\
  b^{n+1} &\ge b && \qqtext{Broadening to a less restrictive comparison}
\end{align*}

This completes the first small diversion.  Next, we have just one more piece to
prove.

\begin{lemma} \lthm{ex-base-inc}
  If $x$, $y$ are real numbers and $n$ is a positive integer, if and only if $x
  \ge y > 1$, then $x^n \ge y^n$.
\end{lemma}

Again, we will need to use induction.  Starting from the base case of $n=1$.

\begin{align*}
  x &\ge y && \qqtext{Given} \\
  x^1 &\ge y^1 && \qqtext{Definition of exponent}
\end{align*}

And the inductive case

\begin{align*}
  x^n &\ge y^n && \qqtext{Inductive case assumption} \\
  x x^n &\ge x y^n && \qqtext{Multiplicative property of inequalities} \\
  x^{n+1} &\ge x y^n && \qqtext{Associativity of multiplication} \\
  &\ge (x - y + y) y^n && \qqtext{Additive inverse} \\
  &\ge (x - y) y^n + y y^n && \qqtext{Distributive property} \\
  &\ge (x - y) y^n + y^{n+1} && \qqtext{Associativity of multiplication} \\
  (x - y) y^n &\ge 0 && \qqtext{Multiplication of two non-negative numbers} \\
  x^{n+1} &\ge (x - y) y^n + y^{n+1} \ge y^{n+1} && \qqtext{Subtracting a non-negative number preserves} \\ 
  &&& \qqtext{the smaller side of an inequality}
\end{align*}

Which completes the induction.  From here, we can finally demonstrate that $b^r$
is monotonic increasing.

\begin{theorem} \lthm{mono-ex}
  Given $b > 1$ and $r$ and $s$ rational, where $r = m/n$ and $s = p/q$, with
  $m$, $n$, $p$, and $q$, with $n$ and $q$ positive, then $b^r > b^s$ if and
  only if $r > s$.
\end{theorem}

First, it is useful to get the rational numbers in terms of the same
denominator.

\begin{align*}
  r &> s && \qqtext{Given} \\
  \frac{m}{n} &> \frac{p}{q} && \qqtext{Transitive property of equality} \\
  \frac{mq}{nq} &> \frac{np}{nq} && \qqtext{Multiplicative identity, inverse, and commutivity} \\
\end{align*}

Next, we can use this inequality to establish the inequality we want.

\begin{align*}
  b^{mq-np} &\ge b && \qqtext{By Lem.~\rthm{ex-inc}} \\
  b^{np} b^{mq-np} &\ge b^{np} b && \qqtext{Multiplication property of inequalities} \\
  b^{np} b &\ge b^{np} && \qqtext{By Lem.~\rthm{mult-inc} and \rthm{ex-inc}} \\
  b^{np} b^{mq-np} &\ge b^{np} && \qqtext{Transitive property of inequalities} \\
  b^{mq} &\ge b^{np} && \qqtext{By Cor.~\rthm{ex-add}} \\
  {\qty({\qty(b^{mq})}^{1/nq})}^{nq} &\ge {\qty({\qty(b^{np})}^{1/nq})}^{nq} && \qqtext{By Cor.~\rthm{self-root-rat}} \\
  {\qty(b^{mq})}^{1/nq} &\ge {\qty(b^{np})}^{1/nq} && \qqtext{By Lem.~\rthm{ex-base-inc}} \\
  b^r &\ge b^s && \qqtext{By Def.~\rthm{rat-ex} and the transitive property of equality}
\end{align*}

Now that we've proven that $b^r \ge b^s$ whenever $r \ge s$ we have just one
thing left to do before we can prove the main result.  We need to prove an
equivalent to the archimedean property but for rational powers of $b$.

\begin{subequations}
  \begin{theorem} \lthm{ex-arch}
    Let $b > 1$, $x > 1$ for real numbers $b$ and $x$, then there exists a
    positive integer $n$ such that

    \[
      b < x^n
    \]
  \end{theorem}
  \begin{theorem} \lthm{ex-dense}
    Let $b > 1$, $x$, and $y$ be real numbers with $x > y$, there exists a
    rational number $r$ such that $x > b^r > y$.
  \end{theorem}
\end{subequations}

For the first theorem, let $A$ be the set of all $x^n$.  If the theorem were false,
then $b$ would be an upper bound of $A$.  But then $A$ has a least upper bound
in $\mathbb{R}$.  Let $\alpha = \sup A$.  Since $x > 1$, $\alpha / x$ < $\alpha$,
and $\alpha/x$ is not an upper bound of $A$. So $\alpha / x < x^m$ for some
positive integer $m$.  But that implies $\alpha < x^{m+1} \in A$, which is
impossible since $\alpha$ is an upper bound of $A$.

For the second theorem, consider the value $x/y > 1$.  By Thm.~\rthm{ex-arch}
there exists some $n$ such that $b < {\qty(x/y)}^n$.

\begin{align*}
  b &< {\qty(\frac{x}{y})}^n && \qqtext{By Thm.~\rthm{ex-arch}} \\
  b y^n &< x^n && \qqtext{Multiplicative property of equality} \\
  y^n < b y^n &< x^n && \qqtext{Property of multiplication} \\
\end{align*}

From here by Thm.~\rthm{ex-arch} there exists some $m$ such that $y^n < b^m$,
choose the least such $m$.  Suppose $b y^n < b^m$, this means $y^n < b^{m-1}$,
but $m$ was the least such integer where that inequality held.  Therefore, $y^n <
b^m < b y^n < x^n$.

\begin{align*}
  y^n &< b^m < x^n && \qqtext{Per the above discussion} \\
  {\qty(y^n)}^{1/n} &< {\qty(b^m)}^{1/n} < {\qty(x^n)}^{1/n} && \qqtext{By Thm.~\rthm{mono-ex}} \\
  y &< b^{m/n} < x && \qqtext{By Cor.~\rthm{self-root-rat} and Def.~\rthm{rat-ex}} \\
\end{align*}

But $m/n$ is a rational number, so we have found a rational number $r$ such that
$x > b^r > y$.

\begin{theorem} \lthm{rat-ex-cuts}
  Let $x$ be a real number and $C(x)$ be the set of all rational numbers less or
  equal to $x$, thus $x = \sup C(x)$. Let $b > 1$ be a real number. Let $B(x)$
  be as defined in the problem statement, the set of all real numbers $b^s$
  where $s$ is rational and $x \ge s$. Given this setup, and $r = m/n$ rational
  with $m$, $n$ integers, and $n$ positive, we have $b^r = \sup B(r)$.
\end{theorem}

Let $s$ be a member of $C(r)$. By Theorem~\rthm{mono-ex}, we have $b^r \ge b^s$
for $b > 1$, thus $b^r$ is an upper bound of $B(r)$.

Suppose there is some smaller upper bound $b^{r} - \varepsilon$ with
$\varepsilon > 0$.

\begin{align*}
  b^r &> b^r - \varepsilon && \qqtext{Given} \\
  b^r > b^s &> b^r - \varepsilon && \qqtext{By Thm.~\rthm{ex-dense}} \\
  r &> s && \qqtext{By Thm.~\rthm{mono-ex}}
\end{align*}

From this $b^s$ must also be an upper bound of $B(r)$.  However, because $b^r >
b^s$, we can see that $s \in B(r)$, because $r > s$.  So $b^{r} - \varepsilon$
cannot be an upper bound of the set, because it is less than a member of the
set.  Therefore not only is $b^r$ and upper bound, it is the least upper bound.

This proves case 2.

\textbf{Case 3:} In this case, we will be dealing with the case where $0 < b <
1$. In this case, we will let $c = 1/b$, and repeat the proof in Case 2.
Resulting in for this range of $b$

\[
  b^r = \inf -B(-r)
\]

Where $-B(x)$ is definied analogously to Problem~\rprob{1}{5}, $-B(x) = \{b^{-t}
: b^t \in B(x)\}$.

This completes the part, and now we can define.

\begin{definition} \lthm{real-ex}
  Given $x$ real, and $B(x)$ the set $\{b^r : r <= x\}$, if $b > 1$ we define
  $b^x \equiv \sup B(x)$, if $0 < b < 1$ we define $b^x \equiv \inf {-B(-x)}$,
  if $b = 1$ then $b^x \equiv 1$ and finally if $b = 0$ then $b^x \equiv 0$ for
  $x \ne 0$.  The value $0^0$ is undefined.
\end{definition}

The last is left undefined due to a conflict between the $0^x = 0$ and $x^0 = 1$
rules.

Before moving on, we need to prove one more slightly stronger version of a
previous theorem.  This time we're going to be proving a strong version of
Thm.~\rthm{mono-ex} by extending it to real numbers.

\begin{theorem} \lthm{mono-ex-real}
  Given $b > 1$ and $x$ and $y$ real, then $b^x > b^y$ if and only if $x > y$.
\end{theorem}

\begin{align*}
  b^x &> b^y && \qqtext{Given} \\
  \sup B(x) &> \sup B(y) && \qqtext{By Def.~\rthm{real-ex}} \\
  b^r &> b^s && \qqtext{For some $b^r \in{} B (x)$ and $b^s \in{} B (y)$} \\
  r &> s && \qqtext{By Thm.~\rthm{mono-ex}} \\
  \mathrm{Let}\: A(x) &= \{r : r \in \mathbb{Q}, r \le x\} && \qqtext{Definition of a Dedekind cut} \\
  \sup A(x) &> \sup A(y) && \qqtext{Implied from $r > s$ and the definition of $\sup$} \\
  x &> y && \qqtext{From the definition of real numbers in Book Chapter 1 Appendix}
\end{align*}

\smallskip \hrule \smallskip

\prompt{(d) Prove that $b^{x+y} = b^x b^y$ for real $x$ and $y$.}

Similar to last section, let's do the trivial cases first, then the main case,
then the final case as a corollary to the main one.

\textbf{Case 1:} Let $b = 0$ or $b = 1$, and $x$ and $y$ real numbers, if $b =
0$, $x$ and $y$ are subjected to the restrictions that $x \ne 0$, $y \ne 0$, and
$x + y \ne 0$.  In this case, because $b^a = b$ for any choice of valid real $a$,

\begin{align*}
  b^x b^y &= b b && \qqtext{By Def.~\rthm{real-ex}} \\
  &= b && \qqtext{Property of the arithmetic identity or multiplicative identity} \\
  &= b^{x+y} && \qqtext{By Def.~\rthm{real-ex}}
\end{align*}

\textbf{Case 2:} Let $b > 1$.

For this case, we will show that the set $B(x+y)$ has a least upper bound equal
to $\sup B(x) \sup B(y)$.  So by Definition~\rthm{real-ex}, $b^x b^y = b^{x+y}$.

First, let's construct a particular set.

\begin{lemma} \lthm{mult-supr-one}
  Let $A$ be a set and $y = \sup A$ be the least upper bound of that set.  Let
  $x > 0$ be a real number.  If we construct the set $B = \{ax : a \in
  A\}$, then $xy = \sup B$.
\end{lemma}

To prove that $xy$ is an upper bound, let's assume that there exists some $b \in
B$ such that $b > xy$.

\begin{align*}
  b &> xy && \qqtext{Assumed} \\
  \frac{b}{x} &> y && \qqtext{By Book Prop. 1.18(b)} \\
  \frac{ax}{x} &> y && \qqtext{For some $a \in{} A$} \\
  a &> y && \qqtext{Multiplicative inverse}
\end{align*}

This implies that $a > y$, but $a$ is an element of $A$, and so an element of
$a$ is larger than the $\sup A$, which is a contradiction.  Therefore $xy$ is an
upper bound.

To prove it's the least such upper bound, let's assume that there exists some
$\varepsilon > 0$ such that $xy - \varepsilon$ is an upper bound of $B$.

\begin{align*}
  xy - \varepsilon &= xy - \frac{x \varepsilon}{x} && \qqtext{Multiplicative Inverse} \\
  &= x \qty(y - \frac{\varepsilon}{x}) && \qqtext{Distributive property} \\
\end{align*}

Consider the value $y - \frac{\varepsilon}{x}$.  This value is not an upper
bound of $A$, because it is less than $y$, which is the least upper bound.  So
there exists some $a \in A$ satisfying $y - \frac{\varepsilon}{x} < a \le y$.

\begin{align*}
  xy - \varepsilon &< ax && \qqtext{From above discussion} \\
  &< b && \qqtext{For some $b \in{} B$}
\end{align*}

Which contradicts are assumption that $xy - \varepsilon$ is an upper bound.
Therefore, $xy$ is the least upper bound, completing the proof.

\begin{theorem} \lthm{mult-supr}
  Let $A$ and $B$ be sets consisting of positive real numbers, with $x = \sup A$
  and $y = \sup B$.  Let $C = \{ab : a \in A, b \in B\}$, then $xy = \sup
  C$.
\end{theorem}

To prove, we can just repeatedly apply Lem.~\rthm{mult-supr-one}.

\begin{align*}
  \mathrm{Let}\: X(c) &= \{ac : a \in A, c \in \mathbb{R}^+\} && \qqtext{Set axiom} \\
  \mathrm{Let}\: Y &= {\sup X(b) : b \in B} && \qqtext{Set axiom} \\
  &= \{bx : b \in B\} && \qqtext{By Lem.~\rthm{mult-supr-one}} \\
  \sup Y &= xy && \qqtext{By Lem.~\rthm{mult-supr-one}} \\
\end{align*}

$\sup C$ cannot be less than $\sup Y$, because that would mean some member of
$Y$ would be larger than $\sup C$, which would mean some $X(b)$ would contain a
member larger than $\sup C$, which would mean some $ab > \sup C$, which is a
contradiction to $\sup C$ status of supremum of $C$.  Similarly $\sup C$ cannot
be greater than $\sup Y$, because that would similarly imply some $ab \le \sup Y
< \sup C$, which is again a contractiction.  Therefore $\sup C = \sup Y$, which
completes the proof.

Now, we can show that $B(x+y)$ can be analyzed by the tool we just created.

\begin{align*}
  B(x+y) &= \{b^q : q \in \mathbb{Q}, q \le x + y\} && \qqtext{Definition of $B$} \\
  \mathrm{Let}\: Q(q | x, y) &= \{ (r, s) :  r \in C(x), s \in C(y), r + s = q \} && \qqtext{By set axioms} \\
  B(x+y) &= \{b^{r+s} : (r, s) \in Q(q| x, y), q \in \mathbb{Q}, q \le x + y\} && \qqtext{Transitive property of equality} \\
  &= \{b^{r+s} : r \in C(x), s \in C(y), r + s \le x + y\} && \qqtext{Definition of $Q$} \\
  &= \{b^{r+s} : r \in C(x), s \in C(y)\} && \qqtext{Redundant statement} \\
  &= \{b^r b^s : r \in C(x), s \in C(y)\} && \qqtext{By Thm.~\rthm{rat-ex-add}} \\
  \sup B(x+y) &= \sup \{b^r b^s : r \in C(x), s \in C(y)\} && \qqtext{Transitive property of equality} \\
  &= \sup \{b^r : r \in C(x)\} \sup \{b^s : s \in C(y)\} && \qqtext{By Thm.~\rthm{mult-supr}} \\
  &= \sup B(x) \sup B(y) && \qqtext{Definition of $B$} \\
  b^{x+y} &= b^x b^y && \qqtext{By Def.~\rthm{real-ex}}
\end{align*}

\textbf{Case 3:} Let $0 < b < 1$.

To prove this case, we're simply going to perform a change of variables, and
then show that case 2 holds.  But before we can do that, we need to prove an
analog to the statement we proved in Problem~\rprob{1}{5}.

\begin{lemma} \lthm{supr-infi-neg-ex}
  Let $A$ be a nonempty set of rational numbers which is bounded below. Let $b$
  be a real number with $b > 1$.  Let $B$ be the set $\{b^x : x \in A\}$. Let
  $-B$ be the set of all numbers $b^{-x}$, where $b^{x} \in B$.  Then

  \[
    \sup B = {\qty(\inf -B)}^{-1}
  \]
\end{lemma}

The proof proceeds mostly analogously to how it occured in Problem~\rprob{1}{5}.
First a lemma to the lemma.

\begin{lemma} \lthm{infi-supr-B}
  Given $b > 1$ and $\beta \in \mbb{R}$, if and only if $b^\beta$ is an upper
  bound of $B$, then $b^{-\beta}$ is a lower bound of $-B$.
\end{lemma}

This is a parallel lemma to Lem.~\rfarthm{1}{5}{supr-infi-neg}, and essentially
the proof is as simple as a correspondance between
Lem.~\rfarthm{1}{5}{supr-infi-neg} and this Lemma, using
Thm.~\rthm{mono-ex-real} to move between the different spaces of numbers.

If $b^\beta$ is a lower bound of $B$, then for any $b^r \in B$ it is true that
$b^\beta \le b^r$, by the definition of lower bound.  Therefore, by
Thm.~\rthm{mono-ex}, $\beta \le r$.  In other words, $\beta$ is a lower bound
of $A$.  Therefore, $-\beta$ is an upper bound of $-A$ by
Lem.~\rfarthm{1}{5}{supr-infi-neg}. And again by Thm.~\rthm{mono-ex}, we have
$b^{-\beta} \ge b^{-r}$ for any $b^{-r} \in -B$.  Finally, this is the
definition of an upper bound, proving sufficience.

Similarly, if $b^{\gamma}$ is not a lower bound of $B$, then there exists a $b^r
\in B$ such that $b^\gamma > b^r$.  By exactly the same logic as above you can
conclude that such a $b^{-\gamma}$ is not an upper bound of $-B$, proving
necessity.

From here the proof of Lem.~\rthm{supr-infi-neg-ex} is also a simple
correspondance between it and the result of \rprob{1}{5} using
Thm.~\rthm{mono-ex}.

Consider the set of all upper bounds of $A$, $U(A)$.  Because there is a
one-to-one corresponance between the upper bounds of $A$ and the lower bounds of
$-A$ we can construct the set of all lower bounds of $-A$ by $\{-a : a \in
U(a)\}$.  Let's call this set $L(-A)$, and note that this set is exactly the set
of all lower bounds of $A$.

Let $\alpha = \sup A$.  Because $\alpha$ is the least upper bound of $A$ it is
contained in $U(A)$ and is less than or equal to all elements of $U(A)$.
Therefore $-\alpha \in L(-A)$. Additionally, because $\alpha \le a$ for all $a
\in U(A)$, it is also true that $-\alpha \ge -a$ for all $a \in U(A)$.  But the
$-a$ are also the members of $L(-A)$.

Therefore, we can use Thm.~\rthm{mono-ex} and Lem.~\rthm{infi-supr-B} to show
that for all $a \in A$ because $\beta = b^{\alpha} \ge b^a$, it is also true
that $\beta^{-1} = b^{-\alpha} \le b^{-a}$, which means that $\beta^{-1} = \inf
-B$, and thus $\sup B = {\qty(\inf -B)}^{-1}$.

Now, finally we can prove this last step.

\begin{align*}
  b^{x+y} &= \inf -B(-x-y) && \qqtext{By Def.~\rthm{real-ex}} \\
  \mathrm{Let}\: c &= 1/b \\
  {(1/c)}^{x+y} &= \inf -B(-x-y) && \qqtext{By the transitive property of equality} \\
  c^{-x-y} &= \inf -B(-x-y) && \qqtext{By Cor.~\rthm{rat-neg-ex}} \\
  c^{x+y} &= {\qty(\inf -B(-x-y))}^{-1} && \qqtext{By multiplicative property of equality} \\
  c^{x+y} &= \sup B(x+y) && \qqtext{By Lem.~\rthm{supr-infi-neg-ex}}
\end{align*}

Now we apply the result from Case 2 to show that $c^{x+y} = c^{x} c^{y}$ and we
move these two separately back into $b^{-x}$ and $b^{-y}$, by the exact same
process in reverse.

Which is, finally, the end of this problem.
