\prompt{Let $A$ be a nonempty set of real numbers which is bounded below. Let $-A$ be the set of all numbers $-x$, where $x \in A$. Prove that}

\[
  \inf A = - \sup (-A).
\]

Strategy:

We will use the definition of the $\inf$ and $\sup$ to show that if an element
is a lower bound of $A$ it is the negative of the upper bound of $-A$.  We will
then show that there is no upper bound of $-A$ smaller than the negative of the
greatest lower bound of $A$, which makes it the least upper bound.  These two
steps together are sufficient to prove the identity.

\smallskip \hrule \smallskip

\begin{lemma} \lthm{supr-infi-neg}
  If and only if $\alpha$ is a lower bound of $A$, then $-\alpha$ is an upper
  bound of $-A$.
\end{lemma}

If $\alpha$ is a lower bound of $A$, then for any $a \in A$ it is true that
$\alpha \le a$, by the definition of lower bound.

If $a \in A$ then $-a \in -A$, by the definition of $-A$.

$\alpha \le a$ implies $-\alpha \ge -a$, for any $a \in A$ and $-a \in -A$.  But
this is exactly the condition for $-\alpha$ to be an upper bound of $-A$.

Each step in this proof is bidirectional, so one may follow the steps backwards
to reach the start from the conclusion, so the implication is bidirectional as
well.

\smallskip \hrule \smallskip

Consider the set of all lower bounds $L(A)$.  Because there is a one-to-one
correspondance between the lower bounds of $A$ and the upper bounds of $-A$ we
can construct the set of all upper bounds of $-A$ by $\{-a : a \in L(A)\}$.
Let's call this set $U$, and note that this set is exactly the set of all upper
bounds of $-A$.

Let $\alpha = \inf A$.  Because $\alpha$ is the greatest lower bound of $A$ it
is contained in $L(A)$ and is greater than or equal to all elements of $L(A)$.
Therefore $-\alpha \in U$. Additionally, because $\alpha \ge a$ for all $a \in
L(A)$, it is also true that $-\alpha \le -a$ for all $a \in L(A)$.  But the $-a$
are also the members of $U$.  Therefore, $-\alpha$ is the least upper bound of
$-A$.

If $-\alpha = \sup(-A)$ then it follows that $\alpha = - \sup(-A)$, and because
$\alpha$ is the greatest lower bound of $A$, we finally have $\inf A = -
\sup(-A)$.
