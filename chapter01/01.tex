\prompt{If $r$ is rational ($r \ne 0$) and $x$ is irrational, prove that $r + x$
and $rx$ are irrational.}

Strategy:

In order to do this, we will prove something more general.  That rationals are
closed under addition and multiplication (and equivalently under subtraction and
division).  Then we will use this result to prove the exercise. To avoid an
extended set theoretic discussion of the construction of the integers and the
definition of addition, it will simply be assumed that integers are closed under
addition and multiplication.

\smallskip \hrule \smallskip

Any rational number can be expressed as the ratio of two integers.  Consider two
rational numbers $a = \tfrac{p_a}{q_a}$ and $b = \tfrac{p_b}{q_b}$.  Where the
pairs of $p_i$ and $q_i$ are not necessarily coprime, and $q_i \ne 0$.  Let $a +
b = c$.

\begin{align*}
  a + b &= \frac{p_a}{q_a} + \frac{p_b}{q_b} && \qqtext{Definition of $a$ and $b$} \\
  &= \frac{p_a}{q_a} \cdot 1 + \frac{p_b}{q_b} \cdot 1 && \qqtext{Multiplicative Identity} \\
  &= \frac{p_a}{q_a} \cdot \frac{q_b}{q_b} + \frac{p_b}{q_b} \cdot \frac{q_a}{q_a} && \qqtext{$\tfrac{a}{a} = 1$ for all $a \ne{} 0$} \\
  &= \frac{p_a q_b}{q_a q_b} + \frac{p_b q_a}{q_a q_b} && \qqtext{Commutivity of Multiplication} \\
  &= \frac{p_a q_b + p_b q_a}{q_a q_b} && \qqtext{Distributive property in reverse} \\
  c &= \frac{p_a q_b + p_b q_a}{q_a q_b} && \qqtext{Transitive property of equality}
\end{align*}

Integers are closed under multiplication and addition, so if $p_a$, $p_b$,
$q_a$, and $q_b$ are all integers (which we have defined them to be) then any
combination of them using the operations of addition and multiplication are also
integers.  In particular, both $p_a q_b + p_b q_a$ and $q_a q_b$ are integers.
And because $c$ is a ratio of two integers, $c$ must also be rational.

So for any two rational numbers $a$ and $b$, their sum must necessarily also be
a rational number.  Equivalently, if $a + b = c$, then $a - (-b) = c$, and
because $-b$ is rational whenever $b$ is rational (to prove, simply note that
$-b = \tfrac{-p_b}{q_b}$ and $-p_b$ and $q_b$ are both integers), the rationals
are also closed under subtraction.

For the case of multiplication, if we have $ab = c$, then we have, more simply:

\begin{align*}
  ab &= \frac{p_a}{q_a} \cdot \frac{p_b}{q_b} && \qqtext{Definition of $a$ and $b$} \\
  ab &= \frac{p_a p_b}{q_a q_b} && \qqtext{Commutivity of Multiplication} \\
  c &= \frac{p_a p_b}{q_a q_b} && \qqtext{Transitive property of equality}
\end{align*}

And similar to the discussion before $c$ is a ratio of two integers, and is
therefore rational.  Finally note that, provided $b \ne 0$, $ab = c$ also
implies $\tfrac{a}{b^{-1}} = c$, and $b^{-1} = \frac{q_b}{q_a}$ is rational.  So
the rationals are also closed under division except for division by zero.

Now that we have proven rationals are closed under the four basic mathematical
operations of a field. Let us examine the exercise.

Consider $r + x = a$, where $r$ and $a$ are rational and $x$ is irrational.

\begin{align*}
  r + x &= a && \qqtext{Given} \\
  -r + (r + x) &= -r + a && \qqtext{Additive property of equality} \\
  (-r + r) + x &= -r + a && \qqtext{Associativity of addition} \\
  0 + x &= -r + a && \qqtext{Property of the additive inverse} \\
  x &= -r + a && \qqtext{Property of the additive identity}
\end{align*}

Now we have an equation where two rational numbers are adding up to an
irrational number, but rational numbers are closed under addition, so they can
only add to another rational number.  This is a contradiction, so our assumption
that $a$ is rational must be false.  Therefore $a$ must be irrational.

Similarly, consider $rx = a$, where $r$ and $a$ are rational and $x$ is
irrational.

\begin{align*}
  rx &= a && \qqtext{Given} \\
  r^{-1} (rx) &= r^{-1} a && \qqtext{Multiplicative property of equality} \\
  (r^{-1} r) x &= r^{-1} a && \qqtext{Associativity of multiplication} \\
  1 \cdot x &= r^{-1} a && \qqtext{Property of the multiplicative inverse} \\
  x &= r^{-1} a && \qqtext{Property of the multiplicative identity}
\end{align*}

Now we have an equation where two rational numbers are multiplying to an
irrational number, but rational numbers are closed under multiplication, so they
can only multiply to another rational number.  This is a contradiction, so our
assumption that $a$ is rational must again be false.  Therefore $a$ is
irrational.
