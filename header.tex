\documentclass[12pt]{book}

\usepackage[letterpaper, total={7in, 9.5in}]{geometry}

% Required to support mathematical unicode
\usepackage[warnunknown, fasterrors, mathletters]{ucs}
\usepackage[utf8x]{inputenc}

% Always typeset math in display style
\everymath{\displaystyle}

% Standard mathematical typesetting packages
\usepackage{kpfonts}
\usepackage{baskervald}
\usepackage{amsthm, amsmath, amssymb}
\usepackage{mathrsfs}
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\usepackage{esint}
\usepackage{mathtools}  % Extension to amsmath
\usepackage{bm}
% \usepackage{newtxtext}
% \usepackage{newtxmath}

% Symbol and utility packages
\usepackage[mathcal]{euscript}
\usepackage[nointegrals]{wasysym}
\usepackage{ulem}
\usepackage{xpatch}
\usepackage[super]{nth}
\usepackage{scalerel}
\usepackage{stackengine}
\usepackage{trimclip} % to fix \j, which still has a dot in kpfonts!
\usepackage{enumitem}

% Extras
\usepackage{siunitx}
\let\qty\SI%

\usepackage{physics}  % Lots of useful shortcuts and macros
\usepackage{tensor} % Help typesetting tensors
\usepackage{tikz-cd}  % For drawing commutative diagrams easily
\usepackage{color}  % Add some colour to life
\usepackage{microtype}  % Minature font tweaks
\usepackage{suffix}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\usepackage{blindtext}

% extra units
\DeclareSIUnit{\torr}{\text{Torr}}
\DeclareSIUnit{\mmHg}{\text{mmHg}}
\DeclareSIUnit{\atm}{\text{atm}}
\DeclareSIUnit{\ton}{\text{tons}}
\DeclareSIUnit{\ft}{\text{ft}}

\input{physucks}

% Common shortcuts
\def\mbb#1{\mathbb{#1}}
\def\mfk#1{\mathfrak{#1}}

\def\bN{\mbb{N}}
\def\bC{\mbb{C}}
\def\bR{\mbb{R}}
\def\bQ{\mbb{Q}}
\def\bZ{\mbb{Z}}

% Sometimes helpful macros
\newcommand{\func}[3]{#1\colon#2\to#3}
\newcommand{\vfunc}[5]{\func{#1}{#2}{#3},\quad#4\longmapsto#5}
\newcommand{\floor}[1]{\left\lfloor#1\right\rfloor}
\newcommand{\ceil}[1]{\left\lceil#1\right\rceil}

% ===== Define a preface environment =====
\newcommand{\prefacename}{Preface}
\newenvironment{preface}{
    \vspace*{\stretch{2}}
    {\noindent \bfseries \Huge \prefacename}
    \begin{center}
        \phantomsection \addcontentsline{toc}{chapter}{\prefacename} % enable this if you want to put the preface in the table of contents
        \thispagestyle{plain}
    \end{center}%
}
{\vspace*{\stretch{5}}}

\newcounter{problem}
\numberwithin{problem}{chapter}

% Some standard theorem definitions
\newtheorem{theorem}{Theorem}[problem]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}

\theoremstyle{definition}

% Some useful statistics shortcuts

% argmin argmax: puts the _ in the right spot
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

% KL divergence: \kldiv{p(x)}{q(x)} gives KL(p(x)||q(x)) typeset right
\DeclarePairedDelimiterX{\divx}[2]{(}{)}{%
  #1\;\delimsize\|\;#2%
}
\newcommand{\kldiv}{KL\divx}

% dependent probabilities: p \giventhat{x}{y} gives p(x|y) typeset right
\makeatletter
\newcommand{\@giventhatstar}[2]{\left(#1\;\middle|\;#2\right)}
\newcommand{\@giventhatnostar}[3][]{#1(#2\;#1|\;#3#1)}
\newcommand{\giventhat}{\@ifstar\@giventhatstar\@giventhatnostar}
\makeatother

% automatic pretty integrals, these functions mostly exist to ensure
% consistency, and so that if I change my mind about what a "pretty" integral
% looks like, I can easily change all of them very quickly.
\newcommand{\pint}[5][\int]{#1_{#2}^{#3} #4 \dd{#5}}
\newcommand{\fpint}[6][\int]{#1_{#2}^{#3} \frac{#4 \dd{#6}}{#5}}

% automated equation naming to avoid conflicts on short names across many different files
\newcommand{\labelprefix}{\prependchapter-\prependproblem}
\newcommand{\leqn}[1]{\label{eq:\labelprefix-#1}}
\newcommand{\reqn}[1]{\ref{eq:\labelprefix-#1}}
\newcommand{\rprob}[2]{\ref{sec:\prependzero{#1}-\prependzero{#2}}}
\newcommand{\lfig}[1]{\label{fig:\labelprefix-#1}}
\newcommand{\rfig}[1]{\ref{fig:\labelprefix-#1}}
\newcommand{\lthm}[1]{\label{thm:\labelprefix-#1}}
\newcommand{\rthm}[1]{\ref{thm:\labelprefix-#1}}
\newcommand{\rfarthm}[3]{\ref{thm:\prependzero{#1}-\prependzero{#2}-#3}}

% some utility functions for including problems easier
\newcommand{\prependzero}[1]{\ifnum#1<10 0\fi#1}
\newcommand{\prependchapter}{\prependzero{\arabic{chapter}}}
\newcommand{\prependproblem}{\prependzero{\arabic{problem}}}
\newcommand{\inputsol}{
  \addtocounter{problem}{1}
  \setcounter{theorem}{0}
  \vfill
  \section{} \label{sec:\labelprefix}
  \input{chapter\prependchapter/\prependproblem.tex}
  \hspace*{\fill} $\blacksquare$
}
\newcommand{\prompt}[1]{\medskip \noindent \textbf{\textit{#1}} \medskip}
\newcommand{\nextchapter}[1]{\setcounter{problem}{0} \chapter{#1}}

% allow equations to break across pages
\allowdisplaybreaks{}
